<%--
  Created by IntelliJ IDEA.
  User: wiloon
  Date: 15-2-11
  Time: 下午10:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
    <script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.js"></script>

    <script>
        $(function () {
            var ctx = "${pageContext.request.contextPath}";

            $("#test").click(function () {
                $.get(ctx + "/text", function () {
                    $("#message").text("sync success.");
                    console.log("sync request success.");
                });
            });

            $("#sync").click(function () {
                $("#message").text("sync request sent.")
                $.get(ctx + "/sync", function () {
                    $("#message").text("sync success.");
                    console.log("sync request success.");
                });
            });

            //get news list
            $("#getNewsList").click(function () {
                $.getJSON(ctx + "/getNewsList", {}, function (data) {
                    console.log("get news list success");
                    //refresh news list
                    console.log("news list size=" + data.length);

                    $("#news").find("ul").find("li").remove();

                    $.each(data, function (i, v) {
                        var description = v.description;
                        console.log("index=" + i + ", id=" + v.id + "description length=" + description.length);
                        // var maxLength=500;
                        // if(description.length>maxLength){
                        // description=description.substr(0,maxLength);
                        // }
                        var activeClass="";
                        if(i==0){
                            activeClass="activeArticle";
                        }else{
                            activeClass="";
                        }
                        var htmlStr = "<article class='articleItem "+activeClass+"'><div class='title'><a href='" + v.link + "'>" + v.title + "</a></div><div class='description'>" + description + "</div></article>"
                        $("#newsList").append(htmlStr);

                    });
                });
            });

            $(window).scroll(function(){
                var scrollTop = $(window).scrollTop();
                var articleTop = $(".activeArticle").offset().top;
                var height=$(".activeArticle").height();
                console.log("scroll top= "+scrollTop+", height="+height);
                if(scrollTop>articleTop+height){
                    var currentActiveArticle = $(".activeArticle");
                    var nextArticle =$(".activeArticle").next();
                    currentActiveArticle.removeClass("activeArticle");
                    nextArticle.addClass("activeArticle");
                }
            });
        });
    </script>

    <style type="text/css">
        .header {
            border: 1px solid red;
            position: fixed;
            top: 0;
            left: 0;
            right:0;
            height: 60px;
            z-index: 100;
            backface-visibility: hidden;
        }


        .nav{
            border: 1px solid green;
            width:280px;
            position:fixed;
            top:60px;
            left:0px;
            bottom:0px;
            z-index: 90;
        }
        .content{
            border: 1px solid blue;
            margin-top:60px;
            margin-left:280px;
            position: relative;
            z-index: 80;
        }
        .articleItem{
            border:1px solid black;
        }
    </style>
</head>
<body>
<div id="app">
    <header class="header  " >
        <button id="test" value="sync ajax">test</button>
        <button id="sync" value="sync ajax">sync</button>
        <button id="getNewsList" value="sync ajax">get news list</button>
    </header>
    <nav class="nav">

nav
    </nav>
    <div class="content">
        Home.jsp
        <div id="message"></div>

        <div id="newsList">

        </div>
    </div>




</div>

</body>
</html>
