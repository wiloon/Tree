package com.wiloon.tree;

/**
 * Created by wiloon on 15-2-11.
 */

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    NewsDao newsDao;

    @PersistenceContext
    private EntityManager em;

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public List<News> sync() {
        FeedReader feedReader = new FeedReader();
        return feedReader.sync();
    }

    @Transactional
    public void save(News news) {
        this.em.persist(news);
    }

    @Transactional
    public void save(List<News> newsList){
        for(News news:newsList){
            this.save(news);
        }
    }

    @Override
    public List<News> readAllNewsList() {
        return newsDao.readAllNews();
    }

    @Override
    public List<News> readNewsByUrl(String url) {

        return newsDao.readNewsByUrl(url);
    }
}
