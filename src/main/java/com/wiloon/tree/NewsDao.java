package com.wiloon.tree;

import java.util.List;

/**
 * Created by wiloon on 15-2-22.
 */
public interface NewsDao {
    public List<News> readAllNews();
    List<News> readNewsByUrl(String url);
}
