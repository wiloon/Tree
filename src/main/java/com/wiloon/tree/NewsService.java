package com.wiloon.tree;

import java.util.List;

/**
 * Created by wiloon on 15-2-21.
 */
public interface NewsService {
    public List<News> sync();
    public void save(News news);

    void save(List<News> newsList);

    List<News> readAllNewsList();
    List<News> readNewsByUrl(String url);
}
