package com.wiloon.tree;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by wiloon on 15-2-11.
 */

@Controller
public class NewsController {
    private static Logger logger = org.slf4j.LoggerFactory.getLogger(NewsController.class);
    @Autowired
    private NewsServiceImpl newsService;

    @RequestMapping(value = "sync")
    public String sync() {
        logger.debug("sync");
       List<News> newsList =  newsService.sync();
        newsService.save(newsList);
        return "home";
    }


    @RequestMapping(value = "test")
    public String test() {
        logger.debug("test request");
        News news = new News();
        news.setTitle("testTitle");
        news.setLink("test link");
        news.setDescription("test description");

        newsService.save(news);
        return "home";
    }

    @RequestMapping(value="getNewsList",method= RequestMethod.GET )
    @ResponseBody
    public List<News> getNewsList(){
        logger.debug("get news list.");
        //get news list from db
        List<News> newsList = newsService.readAllNewsList();
        return newsList;
    }
}
