import com.wiloon.tree.News;
import com.wiloon.tree.NewsService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by wiloon on 15-2-22.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext-repository.xml", "classpath:applicationContext.xml"})
public class NewsServiceTest {
    Logger logger = LoggerFactory.getLogger(NewsServiceTest.class);
    @Autowired
    NewsService newsService;

    @Test
    public void Test() {
        List<News> newsList = newsService.readAllNewsList();
        logger.debug("news list size=" + newsList.size());
    }

    @Test
    public void Test0() {
        List newsList = (List) newsService.readNewsByUrl("http://my.oschina.net/xxiaobian/blog/379500");
        logger.debug("news list size=" + newsList.size());
    }
}
